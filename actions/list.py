from actions.action import AbstractAction

from utils.bot import Bot, Status
from utils.bot_list import bot_list


class List(AbstractAction):

    @staticmethod
    def command():
        return "/list"

    @staticmethod
    def command_short():
        return "/li"

    @staticmethod
    def help_description():
        return "Lister les bots enregistres et leur statut"

    @staticmethod
    def help_args():
        return ["nom"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 1:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        bots = {Status.ERROR: [], Status.READY: [], Status.RUNNING: [], Status.UPDATING: []}

        for bot in bot_list.bots:
            bots[bot_list.get_bot(bot).get_status()].append(bot)

        msg = ""

        if len(bots[Status.READY]) > 0:
            msg += "Bots prets :\n```"
            for bot in bots[Status.READY]:
                msg += bot + "\n"
            msg += "```"

        if len(bots[Status.RUNNING]) > 0:
            msg += "Bots en cours d'execution :\n```"
            for bot in bots[Status.RUNNING]:
                msg += bot + "\n"
            msg += "```"

        if len(bots[Status.UPDATING]) > 0:
            msg += "Bots en cours d'update :\n```"
            for bot in bots[Status.UPDATING]:
                msg += bot + "\n"
            msg += "```"

        if len(bots[Status.ERROR]) > 0:
            msg += "Bots en erreur :\n```"
            for bot in bots[Status.ERROR]:
                msg += bot + "\n"
            msg += "```"

        await message.channel.send(msg)
