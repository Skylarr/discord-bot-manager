from actions.action import AbstractAction

from utils.bot import Bot, Status
from utils.bot_list import bot_list


class Kill(AbstractAction):

    @staticmethod
    def command():
        return "/kill"

    @staticmethod
    def command_short():
        return "/k"

    @staticmethod
    def help_description():
        return "Eteindre un bot"

    @staticmethod
    def help_args():
        return ["nom"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 2:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        if not bot_list.has_bot(splitted[1]):
            await message.channel.send("*Le bot specifie n'est pas enregistre*")
            return

        bot = bot_list.get_bot(splitted[1])
        if not bot.get_status() == Status.RUNNING:
            await message.channel.send("*Le bot specifie n'est pas lance*")
            return

        async with message.channel.typing():
            rc = bot.kill()
        if rc:
            await message.channel.send("*Erreur a l'arret du bot*")
            return

        await message.channel.send("*Bot " + bot.name + " arrete !*")
