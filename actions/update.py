from actions.action import AbstractAction

from utils.bot import Bot, Status
from utils.bot_list import bot_list


class Update(AbstractAction):

    @staticmethod
    def command():
        return "/update"

    @staticmethod
    def command_short():
        return "/u"

    @staticmethod
    def help_description():
        return "Mettre un bot a jour"

    @staticmethod
    def help_args():
        return ["nom"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 2:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        if not bot_list.has_bot(splitted[1]):
            await message.channel.send("*Le bot specifie n'est pas enregistre*")
            return

        bot = bot_list.get_bot(splitted[1])
        if not bot.get_status() == Status.READY:
            await message.channel.send("*Le bot specifie n'est pas pret a etre mis a jour*")
            return

        async with message.channel.typing():
            rc = bot.update()
        if rc:
            await message.channel.send("*Erreur a la mise a jour du bot*")
            return

        await message.channel.send("*Bot " + bot.name + " mis a jour !*")
