from actions.action import AbstractAction

from utils.bot import Bot
from utils.bot_list import bot_list


class Add(AbstractAction):

    @staticmethod
    def command():
        return "/add"

    @staticmethod
    def command_short():
        return "/a"

    @staticmethod
    def help_description():
        return "Ajouter un bot au manager"

    @staticmethod
    def help_args():
        return ["nom path"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) != 3:
            await message.channel.send("*Nombre d'arguments invalide*")
            return

        bot = Bot(splitted[1], splitted[2])
        if bot.is_errored():
            msg = ""
            if bot.is_errored() == 1:
                msg = "*Le chemin donne n'existe pas ou n'est pas un dossier*"
            elif bot.is_errored() == 2:
                msg = "*Le chemin donne n'a pas d'executable correct*"
            elif bot.is_errored() == 3:
                msg = "*Le chemin donne n'a pas de token*"
            await message.channel.send(msg)
            return

        if bot_list.has_bot(bot.name):
            await message.channel.send("*Un bot ayant le nom " + bot.name + " est deja enregistre*")
            return

        bot_list.add_bot(bot)
        await message.channel.send("*Bot " + bot.name + " enregistre !*")
