import json
import os
from json import JSONEncoder

from utils.bot import Bot


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


class BotList:
    def __init__(self, save_path="botlist.json"):
        self.bots = {}
        self.save_path = save_path

    def add_bot(self, bot):
        self.bots[bot.name] = bot
        self.save()

    def has_bot(self, name):
        return name in self.bots

    def get_bot(self, name):
        if not self.has_bot(name):
            return None
        return self.bots[name]

    def save(self):
        file = open(self.save_path, "w")
        file.write(json.dumps(self, cls=MyEncoder))
        file.close()

    def load(self):
        if not os.path.exists(self.save_path):
            return

        file = open(self.save_path, "r")
        data = json.loads(file.read())
        file.close()

        bots = data["bots"]

        for bot_name in bots:
            bot = Bot(bot_name, bots[bot_name]["path"])
            if not bot.is_errored():
                self.add_bot(bot)


bot_list = BotList()
