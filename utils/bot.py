import os
import subprocess
import time

from enum import IntEnum


class Status(IntEnum):
    ERROR = -1
    READY = 0
    RUNNING = 1
    UPDATING = 2


class Bot:
    def __init__(self, name, path):
        self._rc = 0
        self.name = name
        self._status = Status.READY

        if not os.path.isdir(path):
            self._status = Status.ERROR
            self._rc = 1  # Given path does not exist
            return

        self.path = os.path.abspath(path)
        if not os.path.exists(self.path + "/" + name.lower() + ".py"):
            self._status = Status.ERROR
            self._rc = 2  # No executable
            return

        if not os.path.exists(self.path + "/token"):
            self._status = Status.ERROR
            self._rc = 3  # No token
            return
        token_file = open(self.path + "/token", "r")
        self.token = token_file.read().replace(' ', '').replace('\n', '')
        token_file.close()

        self.updatable = os.path.isdir(self.path + "/.git")
        self._process = None

    def get_status(self):
        return self._status

    def is_errored(self):
        return self._rc

    def launch(self):
        if self._status != Status.READY:
            return 1  # Bot is not ready

        self._process = subprocess.Popen(["python3", self.name.lower() + ".py", self.token], cwd=self.path)
        time.sleep(5)  # Wait for bot to launch
        if self._process.poll() is not None:
            return 2
        self._status = Status.RUNNING
        return 0

    def kill(self):
        if self._status != Status.RUNNING:
            return 1  # Bot is not running

        self._process.kill()
        while self._process.poll() is not None:
            continue
        self._status = Status.READY

    def update(self):
        if self._status != Status.READY:
            return 1  # Bot is not ready

        if not self.updatable:
            return 2  # Bot cannot be updated

        self._status = Status.UPDATING

        self._process = subprocess.Popen(["git", "pull"], cwd=self.path)
        while self._process.poll():
            continue

        self._status = Status.READY

        return 0
